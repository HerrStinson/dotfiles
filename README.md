# Personal dotfile repository

As always: provided as is, use at own risk.

# Installation

    yum install -y vim git zsh && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    
    cd ~ && rm -rf .dotfiles && mkdir .dotfiles \
    && curl -o HerrStinson-dotfiles-master.tar.gz https://bitbucket.org/HerrStinson/dotfiles/get/master.tar.gz \
    && tar xzvf HerrStinson-dotfiles-master.tar.gz -C .dotfiles --strip-components 1 && rm -f HerrStinson-dotfiles-master.tar.gz \
    && cd .dotfiles && ./mklinks.sh && cd ..
