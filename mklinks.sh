#!/bin/bash
MY_PATH=`dirname "$0"`
MY_PATH=`( cd "$MY_PATH" && pwd )`

FILES="$MY_PATH/.*"
for f in $FILES
do
  if [[ "$f" != "$MY_PATH/." ]] && [[ "$f" != "$MY_PATH/.." ]] && [[ -f $f ]] && [[ "$f" != "$MY_PATH/.gitconfig" ]]; then
    FILENAME=$(basename $f)
    if ! [[ -h "../$FILENAME" ]] && [[ -f "../$FILENAME" ]]; then
      mv ../$FILENAME ../$FILENAME.bak
    fi  
    ln -s $f .. &> /dev/null
  fi
done


