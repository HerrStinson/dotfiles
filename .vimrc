syntax enable         " enable syntax processing
set tabstop=2		      " number of visual spaces per TAB
set softtabstop=2     " number of spaces in tab when editing
set shiftwidth=2
set expandtab         " tabs are spaces

set number	  		    " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
highlight CursorLine cterm=NONE ctermbg=darkgrey
filetype indent on      " load filetype-specific indent files
set wildmenu            " visual autocomplete for command menu
" set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]

set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
" space open/closes folds
nnoremap <space> za

" nnoremap <F8> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" key bindings: misc
nnoremap <F2> :set invpaste paste?<CR>
nnoremap <F3> :NumbersToggle<CR>
nnoremap <F4> :NumbersOnOff<CR>
nnoremap <F5> :%retab!<CR>
nnoremap <F6> :set cursorline! cursorcolumn!<CR>
" key bindings: split handling
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

augroup vagrant
    au!
    au BufRead,BufNewFile Vagrantfile set filetype=ruby
    au BufRead,BufNewFile Vagrantfile.old set filetype=ruby
    au BufRead,BufNewFile Vagrantfile.locked set filetype=ruby
    au BufRead,BufNewFile Vagrantfile.ori set filetype=ruby
augroup END
